package com.dxy.data

import java.text.SimpleDateFormat
import java.util.Date

import com.dxy.data.hbase.daos.{PiDoctorBaseInfluenceDao, PiDoctorIndexDao, PiUserInfoDao}
import com.dxy.data.hbase.models.{PiDoctorBaseInfluence, PiDoctorIndex}
import com.dxy.data.hbase.{HBaseContent, HBaseUtil}

/**
  * Created by jianghao on 17/2/27.
  */
class PiDoctorIndexTest extends BaseTest {

  val userId = "firefly0512"
  val day = new SimpleDateFormat("yyyy-MM-dd").parse("2017-03-02")

  override def beforeEach() {
    HBaseUtil.createTable(HBaseContent.piDoctorIndexTableName, HBaseContent.piDoctorIndexColumnFamily)

    val doctorIndexScoreMap = Map[String, PiDoctorIndex](
      "10002-4#list_board-post" -> PiDoctorIndex("10002-4_1", 5, 0, 0, 5),
      "10002-4#show_post-topic" -> PiDoctorIndex("10002-4_2", 4, 2, 0, 4),
      "10002-4#event_post-new" -> PiDoctorIndex("10002-4_7-1", 2, 1, 1, 2)
    )
    new PiDoctorIndexDao().putDoctorIndexDay(userId, day, doctorIndexScoreMap)

    new PiDoctorBaseInfluenceDao().putDoctorBaseInfluenceDay(userId, day, PiDoctorBaseInfluence(1,2,3,4,5, day.getTime))
  }

  "dxyid" should "get day's total doctorIndex success" in {
//    val doctorIndex = new PiDoctorIndexDao().getDayDoctorIndexTotalByUserId(userId, day)
//    doctorIndex should have size 1
//    doctorIndex.get.activity_score should be equals 19
//    doctorIndex.get.professional_score should be equals 4
//    doctorIndex.get.influence_score should be equals 2

    val day = new SimpleDateFormat("yyyy-MM-dd").parse("2017-03-02")

    val results = HBaseUtil.scanDataBySmallPageNum(HBaseContent.piDoctorIndexTableName, 1, 10)
    results.foreach(result =>{
      HBaseUtil.showCell(result)
    })



  }


}