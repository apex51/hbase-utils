package com.dxy.data

import java.text.SimpleDateFormat

import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import com.dxy.data.hbase.daos.PiDocumentUserActionDao
import org.scalatest.BeforeAndAfterAll

/**
  * Created by jianghao on 2/28/17.
  */
class PiDocumentUserActionTest extends BaseTest with BeforeAndAfterAll {

  val piDocumentUserActionDao = new PiDocumentUserActionDao()
  val documentId = "bbs_thread|21912"
  val day = new SimpleDateFormat("yyyy-MM-dd").parse("2017-03-01")

  override def beforeAll(): Unit = {

    HBaseUtil.createTable(HBaseContent.piDocumentUserActionTableName,
                          HBaseContent.piDocumentUserActionColumnFamily)

    // 用户 txgeek88077 创作了帖子: bbs_thread|21909 和 bbs_thread|21912
    piDocumentUserActionDao.putDocumentUserAction(
      "bbs_thread|21909",
      day,
      HBaseContent.TAG_TYPE_MC,
      "d5d7df36558a4abba55f446e4d89356d",
      "show_post-topic",
      "10002-4")
    piDocumentUserActionDao.putDocumentUserAction(
      "bbs_thread|21912",
      day,
      HBaseContent.TAG_TYPE_MC,
      "d5d7df36558a4abba55f446e4d89356d",
      "show_post-topic",
      "10002-4")
    piDocumentUserActionDao.putDocumentUserAction(
      "bbs_thread|21912",
      day,
      HBaseContent.TAG_TYPE_MC,
      "d5d7df36558a4abba55f446e4d89356d",
      "show_post-topic",
      "10002-4")
    piDocumentUserActionDao.putDocumentUserAction(
      "bbs_thread|21912",
      day,
      HBaseContent.TAG_TYPE_MC,
      "d5d7df36558a4abba55f446e4d89356d",
      "show_post-topic",
      "10002-4")
    piDocumentUserActionDao.putDocumentUserAction(
      "bbs_thread|21912",
      day,
      HBaseContent.TAG_TYPE_MC,
      "MC_=d5d7df36558a4abba55f446e4d89356d",
      "show_post-topic",
      "10002-4")

  }

  override def afterAll(): Unit = {

//    // 删除测试用例
//    HBaseUtil.delete(HBaseContent.piDocumentUserActionTableName,
//                     HBaseContent.piDocumentUserActionRowkey(
//                       "bbs_thread|21909",
//                       day,
//                       "MC_=d5d7df36558a4abba55f446e4d89356d"))
//    HBaseUtil.delete(HBaseContent.piDocumentUserActionTableName,
//                     HBaseContent.piDocumentUserActionRowkey(
//                       "bbs_thread|21912",
//                       day,
//                       "MC_=d5d7df36558a4abba55f446e4d89356d"))

  }

  override def beforeEach(): Unit = {}

  override def afterEach(): Unit = {}

  "bbs_thread|21912" should "get " in {
    val documentActions = piDocumentUserActionDao
      .findUserActionByDocumentIdAndDate(documentId, day)
    documentActions should have size 1
    documentActions.head.action should startWith("show_post-topic")
  }
}
