package com.dxy.data

import java.text.SimpleDateFormat
import java.util.Date

import com.dxy.data.hbase.daos._
import com.dxy.data.hbase.models.{PiDoctorBaseInfluence, PiDoctorIndex, PiUserLog}
import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import org.apache.hadoop.hbase.client.Put
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import play.api.libs.json.{JsArray, JsNumber, JsString, Json}

import scala.collection.mutable
import scala.util.Random

/**
  * Created by jianghao on 17/2/27.
  */
class BaseTest extends FlatSpec with Matchers with BeforeAndAfterEach {

  val testUserId = "firefly0512"
  // 用户
  val testUserTagType = HBaseContent.TAG_TYPE_MC
  val testUserTag = "d5d7df36558a4abba55f446e4d89356d"
  // 用户 txgeek88077 创作了帖子: bbs_thread|21909 和 bbs_thread|21912
  val testContentUserId = "txgeek88077"
  // 内容的创建者
  val testContentUserTagType = HBaseContent.TAG_TYPE_C
  val testContentUserTag = "1111111222222222333333333"

  val account = "10002-4"
  val contentId1 = "bbs_thread|21909"
  val contentId2 = "bbs_thread|21912"

  val actionId1 = "show_post-topic"
  val actionId2 = "list_board-post"
  val actionId3 = "event_post-new"

  val testDay = new SimpleDateFormat("yyyy-MM-dd").parse("2017-03-03")

  override def beforeEach(): Unit = {
//    HBaseUtil.deleteTable(HBaseContent.piUserInfoTableName)
//    HBaseUtil.deleteTable(HBaseContent.piUserLogTableName)
//    HBaseUtil.deleteTable(HBaseContent.piDoctorIndexTableName)
//    HBaseUtil.deleteTable(HBaseContent.piDoctorBaseInfluenceTableName)
//    HBaseUtil.deleteTable(HBaseContent.piUserContentTableName)
//    HBaseUtil.deleteTable(HBaseContent.piContentTableName)
//    HBaseUtil.deleteTable(HBaseContent.piDocumentUserActionTableName)

    createUserInfo()
    createUserLog()
    createPiContent()
    createDoctorIndex()
    createDoctorBaseInfluence()
    createDocumentUserAction()
  }

  /**
    * 创建 user_info 表，并写入数据
    */
  def createUserInfo(): Unit = {
    HBaseUtil.createTable(HBaseContent.piUserInfoTableName, HBaseContent.piUserInfoColumnFamily)
    HBaseUtil.put(HBaseContent.piUserInfoTableName,
      HBaseContent.piUserInfoRowkey(testUserId, testUserTagType, testUserTag),
      HBaseContent.piUserInfoColumnFamily(0),
      HBaseContent.piUserInfoColumn_userId,
      testUserId
    )
    HBaseUtil.put(HBaseContent.piUserInfoTableName,
      HBaseContent.piUserInfoRowkey(testContentUserId, testContentUserTagType, testContentUserTag),
      HBaseContent.piUserInfoColumnFamily(0),
      HBaseContent.piUserInfoColumn_userId,
      testContentUserId
    )
  }

  /**
    * 创建 user_log 表，并写入数据
    */
  def createUserLog(): Unit = {
    HBaseUtil.createTable(HBaseContent.piUserLogTableName, HBaseContent.piUserLogColumnFamily)
    val logValue = Json.obj(
      "atime" -> new Date().getTime,
      "account" -> account,
      "action" -> actionId1,
      "document_id" -> contentId1,
      "content" -> ""
    )
    HBaseUtil.put(HBaseContent.piUserLogTableName,
      HBaseContent.piUserLogRowkey(testUserTagType, testUserTag, testDay),
      HBaseContent.piUserLogColumnFamily(0),
      (testDay.getTime + random.nextInt(24 * 3600)).toString,
      logValue.toString()
    )
    HBaseUtil.put(HBaseContent.piUserLogTableName,
      HBaseContent.piUserLogRowkey(testUserTagType, testUserTag, testDay),
      HBaseContent.piUserLogColumnFamily(0),
      (testDay.getTime + random.nextInt(24 * 3600)).toString,
      logValue.copy(Map(
        "atime" -> JsNumber(new Date().getTime),
        "action" -> JsString(actionId2),
        "document_id" -> JsString("")
      )).toString()
    )
    HBaseUtil.put(HBaseContent.piUserLogTableName,
      HBaseContent.piUserLogRowkey(testContentUserTagType, HBaseContent.stringToMD5(testContentUserId), testDay),
      HBaseContent.piUserLogColumnFamily(0),
      (testDay.getTime + random.nextInt(24 * 3600)).toString,
      logValue.copy(Map(
        "atime" -> JsNumber(new Date().getTime),
        "action" -> JsString(actionId3),
        "document_id" -> JsString(contentId2)
      )).toString()
    )
  }

  /**
    * 创建 pi_document_content 和 pi_user_content 表，并写入数据
    */
  def createPiContent(): Unit = {
    HBaseUtil.createTable(HBaseContent.piContentTableName, HBaseContent.piContentColumnFamily)
    HBaseUtil.createTable(HBaseContent.piUserContentTableName, HBaseContent.piUserContentColumnFamily)

    val putList = new mutable.MutableList[Put]
    val rowkey = HBaseContent.piContentRowkey(contentId1)
    putList += HBaseUtil.getPutOperation(rowkey, HBaseContent.piContentColumnFamily(0), "id", contentId1)
    putList += HBaseUtil.getPutOperation(rowkey, HBaseContent.piContentColumnFamily(0), "p", "")
    putList += HBaseUtil.getPutOperation(rowkey, HBaseContent.piContentColumnFamily(1), "t", "测试 title")
    putList += HBaseUtil.getPutOperation(rowkey, HBaseContent.piContentColumnFamily(1), "b", "测试 body")
    putList += HBaseUtil.getPutOperation(rowkey, HBaseContent.piContentColumnFamily(1), "o", "测试 other")
    putList += HBaseUtil.getPutOperation(rowkey, HBaseContent.piContentColumnFamily(2), "t", JsArray(Seq(Json.obj("word" -> "感冒"), Json.obj("word" -> "高血压"))).toString())
    putList += HBaseUtil.getPutOperation(rowkey, HBaseContent.piContentColumnFamily(2), "b", JsArray(Seq(Json.obj("word" -> "感冒"), Json.obj("word" -> "高血压"))).toString())
    putList += HBaseUtil.getPutOperation(rowkey, HBaseContent.piContentColumnFamily(2), "o", JsArray().toString())
    // 将内容的创建者放入 hbase
    new PiUserContentDao().putUserContent(
      testContentUserId,
      new SimpleDateFormat("yyyy-MM-dd").parse("2017-03-01"),
      contentId1
    )
    HBaseUtil.batchPut(HBaseContent.piContentTableName, putList.toList)
  }

  val random = new Random()

  /**
    * 创建 doctor_index 表，并写入数据
    */
  def createDoctorIndex(): Unit = {
    HBaseUtil.createTable(HBaseContent.piDoctorIndexTableName, HBaseContent.piDoctorIndexColumnFamily)
    val doctorIndexScoreMap1 = Map[String, PiDoctorIndex](
      account + "#" + actionId1 -> PiDoctorIndex("10002-4_1", random.nextInt(50), 0, 0, 1),
      account + "#" + actionId2 -> PiDoctorIndex("10002-4_2", random.nextInt(50), 0, 0, 4)
    )
    new PiDoctorIndexDao().putDoctorIndexDay(testUserId, testDay, doctorIndexScoreMap1)
    new PiDoctorBaseInfluenceDao().putDoctorBaseInfluenceDay(testUserId,
      testDay,
      PiDoctorBaseInfluence(random.nextInt(100), random.nextInt(100), random.nextInt(20), random.nextInt(10), 0, testDay.getTime))

    val doctorIndexScoreMap2 = Map[String, PiDoctorIndex](
      account + "#" + actionId1 -> PiDoctorIndex("10002-4_1", random.nextInt(50), 0, 0, 1),
      account + "#" + actionId2 -> PiDoctorIndex("10002-4_2", random.nextInt(50), 0, 0, 10),
      account + "#" + actionId3 -> PiDoctorIndex("10002-4_3", random.nextInt(50), 0, 0, 1)
    )
    new PiDoctorIndexDao().putDoctorIndexDay(testContentUserId, testDay, doctorIndexScoreMap2)
  }

  /**
    * 创建 doctor_index_bi 表，并写入数据
    */
  def createDoctorBaseInfluence(): Unit = {
    // 基础影响力
    new PiDoctorBaseInfluenceDao().putDoctorBaseInfluenceDay(testContentUserId,
      testDay,
      PiDoctorBaseInfluence(random.nextInt(100), random.nextInt(100), random.nextInt(20), random.nextInt(10), 0, testDay.getTime))
  }

  /**
    * 创建 pi_document_user_action 表，并写入数据
    */
  def createDocumentUserAction(): Unit = {
    HBaseUtil.createTable(HBaseContent.piDocumentUserActionTableName, HBaseContent.piDocumentUserActionColumnFamily)
    new PiDocumentUserActionDao().putDocumentUserAction(
      contentId1,
      testDay,
      testUserTagType,
      testUserTag,
      actionId1,
      account)
    new PiDocumentUserActionDao().putDocumentUserAction(
      contentId2,
      testDay,
      testUserTagType,
      testUserTag,
      actionId1,
      account)
  }


}
