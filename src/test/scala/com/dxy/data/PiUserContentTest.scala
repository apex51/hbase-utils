package com.dxy.data

import java.util.Date

import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import com.dxy.data.hbase.daos.PiUserContentDao

import scala.util.Random

/**
  * Created by jianghao on 17/2/27.
  */
class PiUserContentTest extends BaseTest {

  val userId = "firefly0512"
  val contentId = "bbs_topic|" + Random.nextInt(1000)
  val day = new Date()


  override def beforeEach() {
    HBaseUtil.createTable(HBaseContent.piUserContentTableName, HBaseContent.piUserContentColumnFamily)
    new PiUserContentDao().putUserContent(userId, day, contentId)
  }

  "dxyid" should "get userContent success" in {
    val userContents = new PiUserContentDao().findContentsByUserId("天天", 2)
    userContents.foreach(content => println(content.atime + ":" + content.documentId))
//    userContents should have size (1)
//    userContents.last should be equals contentId
  }

}
