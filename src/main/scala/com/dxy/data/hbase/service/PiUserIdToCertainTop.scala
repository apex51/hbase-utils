package com.dxy.data.hbase.service

import com.dxy.data.hbase.daos.PiUserIDToKeyWord

/**
  * Created by jianghao on 17/2/7.
  */
class PiUserIdToCertainTop {

  def piUserIdToCertainTop(userId : String ,size: Int = 20 ,daysNum : Int = 7  ): List[(String,Int)] = {
    val piUserIDToKeyWord = new PiUserIDToKeyWord
    val totalWordList = piUserIDToKeyWord.getWordsListFromID(userId ,daysNum)
    if(totalWordList.isEmpty){
      println(s"Do not have this user's labels in this time yet ")
      List((" Do not have this user's labels in this time yet",0))
    } else {
      val wordAndCountSortedList = totalWordList.filter(_ != " ").
          groupBy(f=>f).map(wordAndList=>(wordAndList._1,wordAndList._2.size)).
          toList.sortBy(f=>f._2).
          reverse.
          take(size)
      wordAndCountSortedList
    }
  }
}
