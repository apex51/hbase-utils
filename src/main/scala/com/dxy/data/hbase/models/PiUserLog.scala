package com.dxy.data.hbase.models

import play.api.libs.json.Json

case class PiUserLog(atime: Long = 0,
                     account: String = "",
                     action: String = "",
                     documentId: String = "",
                     content: String = "")

case class PiUserLogDay(rowkey: String,
                        tagType: String,
                        tag: String,
                        timeStr: String,
                        cellsCount: Int = 0,
                        logs: List[PiUserLog] = List())

object PiUserLog {

  implicit val reads = Json.reads[PiUserLog]

  implicit val writes = Json.writes[PiUserLog]

  implicit val format = Json.format[PiUserLog]

}
