package com.dxy.data.hbase.models

import play.api.libs.json.Json

/**
  * Created by jianghao on 17/2/21.
  */
case class PiDoctorIndex(action: String, // 具体产品或行为标识，作为列名
                         activity_score: Int = 0,
                         professional_score: Int = 0,
                         influence_score: Int = 0,
                         times: Int = 0,
                         actionName: String = "", // 存储具有实际意义的产品或行为名称，页面读取使用
                         atime: Long = 0)

// 每个 cell 中存储的结构
case class PiDoctorIndexScore(var activity_score: Int = 0,
                              var professional_score: Int = 0,
                              var influence_score: Int = 0,
                              var times: Int = 0,
                              actionName: String = "" // 存储具有实际意义的产品或行为名称，页面读取使用
                              )

// 每个 rowkey 中的结构
case class PiDoctorIndexDay(rowkey: String,
                            actionIndexs: List[PiDoctorIndex] = List(),
                            timeStr: String)

object PiDoctorIndex {
  implicit val format = Json.format[PiDoctorIndex]
}

object PiDoctorIndexScore {
  implicit val format = Json.format[PiDoctorIndexScore]
}

object PiDoctorIndexDay {
  implicit val formatPiDoctorIndex = Json.format[PiDoctorIndex]
  implicit val format = Json.format[PiDoctorIndexDay]
}
