package com.dxy.data.hbase.models

import play.api.libs.json.Json

/**
  * Created by jianghao on 17/3/7.
  */
// 用户基础影响力（分数）
case class PiDoctorBaseInfluence(fansNumScore: Int = 0, // 粉丝
                                 dingdangNumScore: Int = 0, // 丁当
                                 jobTitleScore: Int = 0, // 职称
                                 hospitalLevelScore: Int = 0, // 医院等级
                                 workYearsScore: Int = 0, // 工作年限
                                 atime: Long = 0)

// 用户基础影响力（因子）
case class PiDoctorBaseInfluenceFactor(fansNum: Int = 0, // 粉丝
                                       dingdangNum: Int = 0, // 丁当
                                       jobTitle: Int = 0, // 职称
                                       hospitalLevel: String = "", // 医院等级
                                       workYears: Int = 0 // 工作年限
                                      )

// 每个 rowkey 中的结构
case class PiDoctorBaseInfluenceDay(rowkey: String,
                                    userId: String,
                                    timeStr: String,
                                    baseInfluenceScore: PiDoctorBaseInfluence)


object PiDoctorBaseInfluence {
  implicit val format = Json.format[PiDoctorBaseInfluence]
}

object PiDoctorBaseInfluenceDay {
  implicit val formatScore = Json.format[PiDoctorBaseInfluence]
  implicit val format = Json.format[PiDoctorBaseInfluenceDay]
}

