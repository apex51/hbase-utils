package com.dxy.data.hbase.models

import play.api.libs.json.Json

/**
  * Created by jianghao on 17/2/27.
  */
case class PiUserContent(rowkey: String,
                         userId: String = "",
                         atime: Long = 0,
                         documentId: String = "")

case class PiUserContentDay(rowkey: String,
                            userId: String = "",
                            timeStr: String,
                            cellsCount: Int,
                            contents: List[PiUserContent] = List())

object PiUserContentDay {
  implicit val format = Json.format[PiUserContent]
  implicit val formatDays = Json.format[PiUserContentDay]
}

object PiUserContent {
  implicit val format = Json.format[PiUserContent]
}
