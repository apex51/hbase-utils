package com.dxy.data.hbase.models

import play.api.libs.json.Json

case class PiUserInfo(rowkey: String,
                      userId: String = "",
                      tagType: String,
                      tag: String = "")

object PiUserInfo {

  implicit val reads = Json.reads[PiUserInfo]

  implicit val writes = Json.writes[PiUserInfo]

  implicit val format = Json.format[PiUserInfo]
}