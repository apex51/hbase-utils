package com.dxy.data.hbase.models

import play.api.libs.json.Json

/**
  * Created by jianghao on 2/28/17.
  */
case class PiDocumentUserAction(rowkey: String,
                                tagType: String,
                                tag: String,
                                atime: Long = 0L,
                                account: String = "",
                                action: String = "",
                                documentId: String = "",
                                content: String = "")

case class PiDocumentUserActionDay(rowkey: String,
                                   documentId: String,
                                   tagType: String,
                                   tag: String,
                                   timeStr: String,
                                   cellsCount: Int,
                                   userActions: List[PiDocumentUserAction] = List(),
                                   var userId: String = "")

object PiDocumentUserActionDay {
  implicit val format = Json.format[PiDocumentUserAction]
  implicit val formatDays = Json.format[PiDocumentUserActionDay]
}

object PiDocumentUserAction {
  implicit val format = Json.format[PiDocumentUserAction]
}