package com.dxy.data.hbase.models

import play.api.libs.json.{JsArray, Json}

case class PiContent(rowkey: String,
                     documentId: String,
                     params: String,
                     piContentOriginal: PiContentOriginal,
                     piContentKeywords: PiContentKeywords)

case class PiContentOriginal(title: String = "", body: String = "", other: String = "")

case class PiContentKeywords(title: JsArray = JsArray(), body: JsArray = JsArray(), other: JsArray = JsArray())

object PiContent {
  implicit val readsPiContentOriginal = Json.reads[PiContentOriginal]
  implicit val writesPiContentOriginal = Json.writes[PiContentOriginal]
  implicit val formatPiContentOriginal = Json.format[PiContentOriginal]

  implicit val readsPiContentKeywords = Json.reads[PiContentKeywords]
  implicit val writesPiContentKeywords = Json.writes[PiContentKeywords]
  implicit val formatPiContentKeywords = Json.format[PiContentKeywords]

  implicit val reads = Json.reads[PiContent]
  implicit val writes = Json.writes[PiContent]
  implicit val format = Json.format[PiContent]
}
