package com.dxy.data.hbase

import org.apache.hadoop.hbase._
import org.apache.hadoop.hbase.client._
import org.apache.hadoop.hbase.filter.{FilterList, PageFilter}
import org.apache.hadoop.hbase.util.Bytes
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.mutable

/**
  * hbase util
  */
object HBaseUtil {
  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  private lazy val conf = HBaseConfiguration.create
  conf.set("hbase.zookeeper.property.clientPort", HBaseContent.zookeeperClientPort)
  conf.set("hbase.zookeeper.quorum", HBaseContent.zookeeperQuorum)
  conf.setInt("hbase.rpc.timeout", 60000)
  conf.setInt("hbase.client.retries.number", 3) // 重试次数
  conf.setInt("hbase.client.pause", 100) // 重试的休眠时间
  conf.setInt("hbase.client.operation.timeout", 30000) //
  conf.setInt("hbase.client.scanner.timeout.period", 20000)

  lazy val conn = ConnectionFactory.createConnection(conf)

  /**
    * 创建表（不强制性）
    *
    * @param tableName   表名
    * @param familyNames 列族列表
    */
  def createTable(tableName: String, familyNames: Array[String]) {
    val admin = conn.getAdmin
    val tn = TableName.valueOf(tableName)
    if (admin.tableExists(tn)) {
      logger.error("table Exists!" + tableName)
    } else {
      val tableDesc = new HTableDescriptor(tn)
      familyNames.foreach(familyName => {
        tableDesc.addFamily(new HColumnDescriptor(familyName))
      })
      admin.createTable(tableDesc)
    }
    admin.close()
  }

  /**
    * 创建表（强制）
    *
    * @param tableName   表名
    * @param familyNames 列族列表
    */
  def createTableForce(tableName: String, familyNames: Array[String]) {
    val admin = conn.getAdmin
    val tn = TableName.valueOf(tableName)
    if (admin.tableExists(tn)) {
      try {
        admin.disableTable(tn)
      } catch {
        case e: TableNotEnabledException => logger.error("表已禁用")
      }
      admin.deleteTable(tn)
    }
    val tableDesc = new HTableDescriptor(tn)
    familyNames.foreach(familyName => {
      tableDesc.addFamily(new HColumnDescriptor(familyName))
    })
    admin.createTable(tableDesc)
    admin.close()
  }

  /**
    * 删表
    *
    * @param tableName 表名
    */
  def deleteTable(tableName: String) {
    val admin = conn.getAdmin
    val tn: TableName = TableName.valueOf(tableName)
    if (admin.tableExists(tn)) {
      try {
        admin.disableTable(tn)
      } catch {
        case e: TableNotEnabledException => {
          logger.error("表已禁用")
        }
      }
      admin.deleteTable(tn)
    }
  }

  /**
    * 插入数据
    *
    * @param tableName 表名
    * @param rowkey    行key
    * @param colFamily 列族
    * @param col       子列
    * @param value     值
    */
  def put(tableName: String, rowkey: String, colFamily: String, col: String, value: String): Unit = {
    val table = conn.getTable(TableName.valueOf(tableName))
    val put: Put = new Put(Bytes.toBytes(rowkey))
    put.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(col), Bytes.toBytes(value))
    table.put(put)
  }

  /**
    * 批量插入多个 rowkey 数据
    */
  def batchPut(tableName: String, rowkeys: Array[String], colFamily: String, col: String, values: Array[String]): Unit = {
    val putList = new mutable.MutableList[Row]
    (rowkeys, values).zipped.foreach((rowkey, value) => {
      val put: Put = new Put(Bytes.toBytes(rowkey))
      put.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(col), Bytes.toBytes(value))
      putList += put
    })
    val results = new Array[Object](putList.size)

    import collection.JavaConversions._
    val table = conn.getTable(TableName.valueOf(tableName))
    table.batch(putList, results)
  }

  def getPutOperation(rowkey: String, colFamily: String, col: String, value: String): Put = {
    val put: Put = new Put(Bytes.toBytes(rowkey))
    put.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(col), Bytes.toBytes(value))
    put
  }

  def batchPut(tableName: String, putList: List[Put]): Unit = {
    val results = new Array[Object](putList.size)
    import collection.JavaConversions._
    val table = conn.getTable(TableName.valueOf(tableName))
    table.batch(putList, results)
  }

//  /**
//    * 批量插入同一个 rowkey 中的多列数据
//    */
//  def batchPutByRowkey(table: Table, rowkey: String, colFamily: String, cols: Array[String], values: Array[String]): Unit = {
//
//  }

  /**
    * 删除数据
    *
    * @param tableName 表名
    * @param rowkey    列key
    * @param colFamily 行族
    * @param col       子列
    */
  def delete(tableName: String, rowkey: String, colFamily: String = null, col: String = null): Unit = {
    val table = conn.getTable(TableName.valueOf(tableName))
    val delete: Delete = new Delete(Bytes.toBytes(rowkey))
    //删除指定列族
    if (colFamily != null && col == null) delete.addFamily(Bytes.toBytes(colFamily))
    //删除指定列
    if (colFamily != null && col != null) delete.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(col))
    table.delete(delete)
  }

  /**
    * 根据 rowkey 查找数据
    *
    * @param tableName 表名
    * @param rowkey    行key
    * @param colFamily 列族
    * @param col       子列
    */
  def get(tableName: String, rowkey: String, colFamily: String = null, col: String = null): Result = {
    val table = conn.getTable(TableName.valueOf(tableName))
    val get: Get = new Get(Bytes.toBytes(rowkey))
    if (colFamily != null && col == null) get.addFamily(Bytes.toBytes(colFamily))
    if (colFamily != null && col != null) get.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(col))
    val result: Result = table.get(get)
    table.close()
    result
  }

  /**
    * 批量根据 rowkey 查找数据
    *
    * @param tableName
    * @param rowkeys
    * @param colFamily
    * @param col
    * @return
    */
  def batchGet(tableName: String, rowkeys: Array[String], colFamily: String = null, col: String = null): Array[Result] = {
    val table = conn.getTable(TableName.valueOf(tableName))
    val getList = new mutable.MutableList[Get]
    rowkeys.foreach(rowkey => {
      val get: Get = new Get(Bytes.toBytes(rowkey))
      if (colFamily != null && col == null) get.addFamily(Bytes.toBytes(colFamily))
      if (colFamily != null && col != null) get.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(col))
      getList += get
    })
    import collection.JavaConversions._
    val results = table.get(getList.toList)
    table.close()
    results
  }

  /**
    * 对于小页码分页，采用舍弃前几页数据的方式分页
    */
  def scanDataBySmallPageNum(tableName: String, currPage: Int, pageSize: Int, filterList: FilterList = null, startRow: String = "", stopRow: String = ""): List[Result] = {
    val table = conn.getTable(TableName.valueOf(tableName))
    val scan: Scan = new Scan
    //    scan.setCaching(1000)
    if (startRow.nonEmpty) {
      scan.setStartRow(Bytes.toBytes(startRow))
    }
    if (stopRow.nonEmpty) {
      scan.setStopRow(Bytes.toBytes(stopRow))
    }

    // 对于小页码分页，采用舍弃前几页数据的方式分页
    val getSize = currPage * pageSize

    //scan.setMaxResultSize(getSize)
    if (filterList != null) {
      filterList.addFilter(new PageFilter(getSize))
      scan.setFilter(filterList)
    } else {
      scan.setFilter(new PageFilter(getSize))
    }

    try {
      val it = table.getScanner(scan)
      val result = it.next(getSize)
      result.toList
    } catch {
      case e: Exception =>
        logger.error(e.getMessage, e)
        logger.error("table scan 访问超时")
        List()
    } finally {
      table.close()
    }
  }

  /**
    * scan 获取一定范围内的所有行，设置上限为 1000000
    */
  def scanData(tableName: String, filterList: FilterList = null, startRow: String = "", stopRow: String = ""): List[Result] = {
    scanDataBySmallPageNum(tableName = tableName, currPage = 1, pageSize = 1000000, filterList = filterList,
      startRow = startRow, stopRow = stopRow)
  }

  def toStr(bt: Array[Byte]): String = {
    if (bt == null) {
      return ""
    }
    Bytes.toString(bt)
  }

  def getBytes(str: String): Array[Byte] = {
    if (str == null) {
      Bytes.toBytes("")
    }
    return Bytes.toBytes(str)
  }

  /**
    * 格式化输出
    *
    * @param result 结果
    */
  def showCell(result: Result): Unit = {
    val cells: Array[Cell] = result.rawCells
    for (cell <- cells) {
      logger.info("RowName:" + new String(CellUtil.cloneRow(cell)) + "")
      logger.info("Timetamp:" + cell.getTimestamp + "")
      logger.info("column Family:" + new String(CellUtil.cloneFamily(cell)) + "")
      logger.info("row Name:" + new String(CellUtil.cloneQualifier(cell)) + "")
      logger.info("value:" + new String(CellUtil.cloneValue(cell)) + "")
      logger.info("---------------")
    }
  }

}
