package com.dxy.data.hbase.daos

import java.util.Date

import com.dxy.data.hbase.models.{PiUserContent}
import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import org.apache.commons.lang.StringUtils
import org.apache.hadoop.hbase.CellUtil
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.Json

import scala.collection.mutable

/**
  * Created by jianghao on 17/2/27.
  */
class PiUserContentDao {

  val logger: Logger = LoggerFactory.getLogger(classOf[PiUserContentDao])

  /**
    * put 用户内容数据
    */
  def putUserContent(userId: String, createDate: Date, documentId: String) = {
    val rowkey = HBaseContent.piUserContentRowkey(userId, createDate)
    if (rowkey.isEmpty) {
      logger.error("user content rowkey is empty:" + userId + ":" + documentId)
    } else {
      // 毫秒数作为列名
      HBaseUtil.put(HBaseContent.piUserContentTableName, rowkey, HBaseContent.piUserContentColumnFamily(0),
        (createDate.getTime % 86400000).toString, Json.obj("atime" -> createDate.getTime, "document_id" -> documentId).toString())
    }
  }

  /**
    * 查询最近 n 天的用户内容
    */
  def findContentsByUserId(userId: String, days: Int): List[PiUserContent] = {
    if (StringUtils.isEmpty(userId)) {
      return List()
    }

    val startRow = HBaseContent.piUserContentRowkeyPrefix(userId)
    val endRow = startRow + "~"
    val results = HBaseUtil.scanDataBySmallPageNum(HBaseContent.piUserContentTableName, 1, days, null, startRow, endRow)

    val contents = new mutable.MutableList[PiUserContent]()
    results.foreach(result => {
      val rowkey = HBaseUtil.toStr(result.getRow)
      result.rawCells().foreach(cell => {
        val valueJson = Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(cell)))
        (valueJson \ "document_id").asOpt[String].getOrElse("") match {
          case documentId if documentId.nonEmpty =>
            contents += PiUserContent(rowkey, userId, (valueJson \ "atime").asOpt[Long].getOrElse(0L), documentId)
          case _ =>
        }
      })
    })
    contents.toList
  }

}
