package com.dxy.data.hbase.daos

import com.dxy.data.hbase.models.{PiContent, PiUserLog}
import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import org.apache.hadoop.hbase.{CellUtil}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.Json

import scala.collection.mutable

/**
  * Created by jianghao on 17/2/7.
  */
class PiUserIDToKeyWord {

  val piContentDao = new PiContentDao()
  val piUserInfoDao = new PiUserInfoDao()

  /**
    * The last certain  days of a user's visit to a corresponding log list of tags
    */
  def findDayLogsByUserLogs(userTags: List[(String, String)], certainDays: Int): mutable.MutableList[PiUserLog] = {
    val dayResult = userTags.map { tag =>
      val startRow = HBaseContent.piUserLogRowkeyPrefix(tag._1, tag._2)
      val endRow = startRow + "~"
      (startRow, endRow)
    }.filter(_._1.nonEmpty).par.flatMap { f =>
      HBaseUtil.scanDataBySmallPageNum(HBaseContent.piUserLogTableName, 1, userTags.length * certainDays, null, f._1, f._2)
    }.seq.toList

    val userLogs = new mutable.MutableList[PiUserLog]()
    dayResult.foreach(result => {
      val rowkey = HBaseUtil.toStr(result.getRow)
      val cellCount = result.size()
      result.rawCells.foreach(cell => {
        try {
          val valueJson = Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(cell)))
          //println((valueJson \ "content").asOpt[String].getOrElse("").toString)

          if ((valueJson \ "document_id").asOpt[String].getOrElse("").nonEmpty) {
            userLogs += PiUserLog(
              (valueJson \ "atime").asOpt[Long].getOrElse(0),
              (valueJson \ "account").asOpt[String].getOrElse(""),
              (valueJson \ "action").asOpt[String].getOrElse(""),
              (valueJson \ "document_id").asOpt[String].getOrElse(""),
              (valueJson \ "content").asOpt[String].getOrElse("")
            )
          }

        } catch {
            case e: Exception =>
            HBaseUtil.logger.error(e.getMessage, e)
        }
      })
    })

    val uLogs = userLogs.sortWith{(piUserLog1:PiUserLog,piUserLog2:PiUserLog) =>
                piUserLog2.atime - piUserLog1.atime <0}

    if(uLogs.isEmpty){
      mutable.MutableList[PiUserLog]()
    }else {
      val latestAtime = uLogs.head.atime
      val beforeCertainDaysAtime = latestAtime - certainDays * 86400000l
      val certainDays_UserLogs = uLogs.filter(_.atime > beforeCertainDaysAtime)
      certainDays_UserLogs
    }
  }

  /**
    * According to userLogs(List) find the corresponding documentIdList
    *
    * @param userLogs
    */
  def getDocument_idListFromUserLogList (userLogs:mutable.MutableList[PiUserLog]): List[String] ={
    val documentIdList = new mutable.MutableList[String]()
    userLogs.foreach(userLog =>{
      //println(new Date(userLog.atime))
      documentIdList += userLog.documentId
    })
    documentIdList.toList
  }

  /**
    * get a wordList corrsponding to the documentId
    *
    *
    * @return
    */
  def wordsList (content:PiContent):List[String] = {
    val words = content.piContentKeywords.title ++ content.piContentKeywords.body ++ content.piContentKeywords.other
    val jsValueSeq = words.value
    // The list used to store the final keyword
    val wordList = new mutable.MutableList[String]()
    if (words.value.length >0){
      for (i <- 0 until words.value.length ){
        val valueJson =jsValueSeq(i)
        wordList += (valueJson \ "word").asOpt[String].getOrElse("")
      }
    }
    wordList.toList
  }

  /**
    * get All of the keyword lists are obtained from documentIdList
    *
    * @param documentIdList
    * @return
    */
  def totalWordsList (documentIdList:List[String]):List[String] ={

    val totalWordList =  new mutable.MutableList[String]()
    val rowkeyList = new mutable.MutableList[String]()
    documentIdList.foreach(documentId => {
      rowkeyList += HBaseContent.piContentRowkey(documentId)
    })

    val resultList = HBaseUtil.batchGet(HBaseContent.piContentTableName, rowkeyList.toArray, "k")

    resultList.foreach(result => {
      piContentDao.getPiContent(result) match {
        case Some(piContent) =>
          val documentIdWordsList = wordsList(piContent)
          totalWordList ++= documentIdWordsList
        case _ =>
      }
    })

    totalWordList.toList
  }

  def getWordsListFromID(userId:String,certainDays:Int ):List[String] = {

    // findTagsByUserId(userId)
    val userTagsList = piUserInfoDao.findTagsByUserId(userId)
    val daysLogs = findDayLogsByUserLogs(userTagsList,certainDays);
    val documentIdList = getDocument_idListFromUserLogList(daysLogs)
    val totalWordList = totalWordsList(documentIdList)
    totalWordList
  }

}