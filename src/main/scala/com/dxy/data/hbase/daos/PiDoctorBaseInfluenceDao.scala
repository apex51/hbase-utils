package com.dxy.data.hbase.daos

import java.util.Date

import com.dxy.data.hbase.models.{PiDoctorBaseInfluence, PiDoctorBaseInfluenceDay}
import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import org.apache.hadoop.hbase.CellUtil
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsObject, Json}

/**
  * Created by jiangh on 17/3/7.
  */
class PiDoctorBaseInfluenceDao {

  val logger: Logger = LoggerFactory.getLogger(classOf[PiDoctorBaseInfluenceDao])

  /**
    * 更新 userId 每天的基础影响力
    */
  def putDoctorBaseInfluenceDay(userId: String, day: Date, piDoctorBaseInfluence: PiDoctorBaseInfluence) = {
    val rowkey = HBaseContent.piDoctorBaseInfluenceRowkey(userId, day)
    HBaseUtil.put(HBaseContent.piDoctorBaseInfluenceTableName, rowkey, HBaseContent.piDoctorBaseInfluenceColumnFamily(0),
      HBaseContent.piDoctorBaseInfluenceColumn_baseInfluenceScore, Json.toJson(piDoctorBaseInfluence).toString())
  }

  /**
    * 获得用户某一天的基础影响力
    *
    * @param userId
    * @param day
    * @return
    */
  def getDayInfulenceBaseScoreByUserId(userId: String, day: Date): Option[PiDoctorBaseInfluenceDay] = {
    val rowkey = HBaseContent.piDoctorBaseInfluenceRowkey(userId, day)
    val dayResult = HBaseUtil.get(HBaseContent.piDoctorBaseInfluenceTableName, rowkey,
      HBaseContent.piDoctorBaseInfluenceColumnFamily(0),
      HBaseContent.piDoctorBaseInfluenceColumn_baseInfluenceScore)
    dayResult.rawCells.headOption match {
      case Some(cell) =>
//        val valueJson = Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(cell))).as[JsObject] ++
//          Json.obj("atime" -> cell.getTimestamp)
        val valueJson = Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(cell)))
        Some(PiDoctorBaseInfluenceDay(rowkey, userId, rowkey.split(":")(1),
          valueJson.asOpt[PiDoctorBaseInfluence].getOrElse(PiDoctorBaseInfluence()))
        )
      case None =>
        None
    }
  }

}
