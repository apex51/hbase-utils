package com.dxy.data.hbase.daos

import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import org.apache.commons.lang.StringUtils
import org.slf4j.{Logger, LoggerFactory}

/**
  * Created by jianghao on 17/2/6.
  */
class PiUserInfoDao {

  val logger: Logger = LoggerFactory.getLogger(classOf[PiUserInfoDao])

  /**
    * 通过 userId 找到用户 tag 列表
    */
  def findTagsByUserId(userId: String): List[(String, String)] = {
    if (StringUtils.isEmpty(userId)) {
      return List()
    }
    val startRow = HBaseContent.piUserInfoRowkeyPrefix(userId)
    val endRow = startRow + "~"
    val results = HBaseUtil.scanDataBySmallPageNum(HBaseContent.piUserInfoTableName, 1, 100, null, startRow, endRow)
    results.map(result => {
      HBaseUtil.toStr(result.getRow) match {
        case rowkey if (rowkey.split(":").length == 3) =>
          (rowkey.split(":")(1), rowkey.split(":")(2))
        case _ => ("", "")
      }
    }).filter(_._1.nonEmpty)
  }

}
