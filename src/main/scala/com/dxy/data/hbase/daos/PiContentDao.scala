package com.dxy.data.hbase.daos

import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import com.dxy.data.hbase.models._
import org.apache.hadoop.hbase.client.Result
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsArray, Json}

/**
  * Created by yangy on 17/2/6.
  */
class PiContentDao {

  val logger: Logger = LoggerFactory.getLogger(classOf[PiContentDao])

  /**
    * 根据 documentId 获得 content
    */
  def getContentByDocumentId(documentId: String): Option[PiContent] = {
    val rowkey = HBaseContent.piContentRowkey(documentId)
    val result = HBaseUtil.get(HBaseContent.piContentTableName, rowkey)
    getPiContent(result)
  }

  def getPiContent(result: Result): Option[PiContent] = {
    HBaseUtil.toStr(result.getRow) match {
      case rowkey if rowkey.nonEmpty =>
        val documentId = HBaseUtil.toStr(result.getValue("c".getBytes(), "id".getBytes()))
        val params = HBaseUtil.toStr(result.getValue("c".getBytes(), "p".getBytes()))

        val originalTitle = HBaseUtil.toStr(result.getValue("o".getBytes(), "t".getBytes()))
        val originalBody = HBaseUtil.toStr(result.getValue("o".getBytes(), "b".getBytes()))
        val originalOther = HBaseUtil.toStr(result.getValue("o".getBytes(), "o".getBytes()))
        val keywordTitle = HBaseUtil.toStr(result.getValue("k".getBytes(), "t".getBytes()))
        val keywordBody = HBaseUtil.toStr(result.getValue("k".getBytes(), "b".getBytes()))
        val keywordOther = HBaseUtil.toStr(result.getValue("k".getBytes(), "o".getBytes()))

        Some(PiContent(rowkey, documentId, params,
          PiContentOriginal(originalTitle, originalBody, originalOther),
          PiContentKeywords(
            keywordTitle.nonEmpty match {
              case true => Json.parse(keywordTitle).asOpt[JsArray].getOrElse(JsArray())
              case _ => JsArray()
            },
            keywordBody.nonEmpty match {
              case true => Json.parse(keywordBody).asOpt[JsArray].getOrElse(JsArray())
              case _ => JsArray()
            },
            keywordOther.nonEmpty match {
              case true => Json.parse(keywordOther).asOpt[JsArray].getOrElse(JsArray())
              case _ => JsArray()
            }
          )))
      case "" => None
    }
  }

}
