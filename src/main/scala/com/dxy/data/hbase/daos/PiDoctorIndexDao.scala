package com.dxy.data.hbase.daos

import java.util.Date

import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import com.dxy.data.hbase.models._
import org.apache.hadoop.hbase.CellUtil
import org.apache.hadoop.hbase.client.Put
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsObject, Json}

import scala.collection.mutable

/**
  * Created by jiangh on 17/2/22.
  */
class PiDoctorIndexDao {

  val logger: Logger = LoggerFactory.getLogger(classOf[PiUserLogDao])

  /**
    * 更新 userId 每天的影响力指数
    *
    * @param userId
    * @param day
    * @param doctorInfluenceScoreMap
    */
  def putDoctorInfluenceDay(userId: String, day: Date, doctorInfluenceScoreMap: Map[String, Int]) = {
    val rowkey = HBaseContent.piDoctorIndexRowkey(userId, day)
    val putList = new mutable.MutableList[Put]
    doctorInfluenceScoreMap.foreach(doctorInfluence => {
      val value = PiDoctorIndexScore(0, 0, doctorInfluence._2, 0, doctorInfluence._1 + "#influence")
      putList += HBaseUtil.getPutOperation(rowkey, HBaseContent.piDoctorIndexColumnFamily(0),
        doctorInfluence._1, Json.toJson(value).toString)
    })
    // 更新时间
    putList += HBaseUtil.getPutOperation(rowkey, HBaseContent.piDoctorIndexColumnFamily(0),
      HBaseContent.piDoctorIndexColumn_influenceTime, new Date().getTime.toString)
    HBaseUtil.batchPut(HBaseContent.piDoctorIndexTableName, putList.toList)
  }

  /**
    * 更新 userId 每天的活跃度指数
    *
    * TODO: 该方法待修改，只需要关心活跃度
    */
  //  def putDoctorActivityIndexDay(userId: String, day: Date, doctorIndexScoreMap: Map[String, PiDoctorIndex]) = {
  def putDoctorIndexDay(userId: String, day: Date, doctorIndexScoreMap: Map[String, PiDoctorIndex]) = {
    val rowkey = HBaseContent.piDoctorIndexRowkey(userId, day)
    // 每个 action 分数
    val putList = new mutable.MutableList[Put]
    doctorIndexScoreMap.foreach(doctorIndex => {
      val value = PiDoctorIndexScore(
        doctorIndex._2.activity_score,
        doctorIndex._2.professional_score,
        doctorIndex._2.influence_score,
        doctorIndex._2.times,
        doctorIndex._1)
      putList += HBaseUtil.getPutOperation(rowkey, HBaseContent.piDoctorIndexColumnFamily(0),
        doctorIndex._2.action, Json.toJson(value).toString())
    })
    // 更新时间
    putList += HBaseUtil.getPutOperation(rowkey, HBaseContent.piDoctorIndexColumnFamily(0),
      HBaseContent.piDoctorIndexColumn_activityTime, new Date().getTime.toString)
    HBaseUtil.batchPut(HBaseContent.piDoctorIndexTableName, putList.toList)
  }

  /**
    * 获得 userId 某一天所有 action 的医生指数
    *
    * @param userId
    * @param day
    * @return
    */
  def getDayDoctorIndexByUserId(userId: String, day: Date): Option[PiDoctorIndexDay] = {
    val rowkey = HBaseContent.piDoctorIndexRowkey(userId, day)
    val dayResult = HBaseUtil.get(HBaseContent.piDoctorIndexTableName, rowkey)

    val indexList = dayResult.rawCells.map(cell => {
      val action = HBaseUtil.toStr(CellUtil.cloneQualifier(cell))
      val valueJson = Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(cell)))
      PiDoctorIndex(
        action,
        (valueJson \ "activity_score").asOpt[Int].getOrElse(0),
        (valueJson \ "professional_score").asOpt[Int].getOrElse(0),
        (valueJson \ "influence_score").asOpt[Int].getOrElse(0),
        (valueJson \ "times").asOpt[Int].getOrElse(0),
        (valueJson \ "actionName").asOpt[String].getOrElse(""),
        cell.getTimestamp
      )
    }).toList

    if (indexList.nonEmpty) {
      Some(PiDoctorIndexDay(rowkey, indexList, rowkey.split(":").last))
    } else {
      None
    }
  }

}
