package com.dxy.data.hbase.daos

import java.util.Date

import com.dxy.data.hbase.models.PiDocumentUserAction
import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import org.apache.hadoop.hbase.CellUtil
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.Json

/**
  * Created by jianghao on 2/28/17.
  */
class PiDocumentUserActionDao {

  val logger: Logger =
    LoggerFactory.getLogger(classOf[PiDocumentUserActionDao])

  /**
    * Put 文章对应的用户行为
    */
  def putDocumentUserAction(documentId: String,
                            createDate: Date,
                            tagType: String,
                            tag: String,
                            action: String,
                            account: String) = {
    val rowkey = HBaseContent.piDocumentUserActionRowkey(documentId, createDate, tagType, tag)
    if (rowkey.isEmpty) {
      logger.error(
        "document to user action is empty:" + documentId + ":" + createDate.toString + ":" + tag)
    } else {
      // 每天的相对毫秒数作为列名
      HBaseUtil.put(HBaseContent.piDocumentUserActionTableName,
                    rowkey,
                    HBaseContent.piDocumentUserActionColumnFamily.head,
                    (createDate.getTime % 86400000).toString,
                    Json
                      .obj("atime" -> createDate.getTime,
                           "action" -> action,
                           "document_id" -> documentId,
                           "content" -> "",
                           "account" -> account)
                      .toString())
    }
  }

  /**
    * 查询 document_id 在一天的用户行为
    */
  def findUserActionByDocumentIdAndDate(documentId: String,
                                        date: Date): List[PiDocumentUserAction] = {
    if (documentId.isEmpty) return List()

    val startRow =
      HBaseContent.piDocumentUserActionRowkeyPrefixWithDate(documentId, date)
    val endRow = startRow + "~"
    val result = HBaseUtil.scanData(HBaseContent.piDocumentUserActionTableName,
                                    null,
                                    startRow,
                                    endRow)

    result.flatMap { result =>
      val rowkey = HBaseUtil.toStr(result.getRow)
      if (rowkey.split(":").length == 5) {
        result.rawCells().map { cell =>
          val valueJson = Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(cell)))
          PiDocumentUserAction(
            rowkey,
            rowkey.split(":")(3),
            rowkey.split(":")(4),
            (valueJson \ "atime").asOpt[Long].getOrElse(0L),
            (valueJson \ "account").asOpt[String].getOrElse(""),
            (valueJson \ "action").asOpt[String].getOrElse(""),
            (valueJson \ "documentId").asOpt[String].getOrElse(""),
            (valueJson \ "content").asOpt[String].getOrElse(""))
        }
      } else {
        List()
      }
    }
  }
}
