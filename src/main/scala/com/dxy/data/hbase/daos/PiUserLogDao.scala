package com.dxy.data.hbase.daos

import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import com.dxy.data.hbase.models._
import org.apache.commons.lang.StringUtils
import org.apache.commons.lang.math.NumberUtils
import org.apache.hadoop.hbase.CellUtil
import org.apache.hadoop.hbase.filter._
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.Json

import scala.collection.mutable

/**
  * Created by jianghao on 17/2/6.
  */
class PiUserLogDao {

  val logger: Logger = LoggerFactory.getLogger(classOf[PiUserLogDao])

  /**
    * 每天的日志列表
    */
  def listDayLogs(page: Int = 1, pageSize: Int = 10, keyword: String): List[PiUserLogDay] = {
    val filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL)
    var startRow = ""
    var stopRow = ""
    if (StringUtils.isNotEmpty(keyword)) {
      var prefix = ""
      if (keyword.startsWith("MC:") || keyword.startsWith("C:") || keyword.startsWith("U:")) {
        prefix = HBaseContent.piUserLogRowkeyPrefix(keyword.split(":")(0), keyword.split(":")(1))
      } else if (keyword.startsWith("^")) {
        prefix = keyword.substring(1)
      }
      if (prefix.nonEmpty) {
        logger.info("search prefix:" + prefix)
        //        startRow = prefix
        //        stopRow = prefix.substring(0, prefix.length) + "z"
        //        filterList.addFilter(new InclusiveStopFilter(Bytes.toBytes(stopRow)))
        filterList.addFilter(new PrefixFilter(prefix.getBytes()))
      } else {
        filterList.addFilter(new RowFilter(CompareFilter.CompareOp.EQUAL, new SubstringComparator(keyword)))
      }
    }

    val resultList = HBaseUtil.scanDataBySmallPageNum(HBaseContent.piUserLogTableName, page, pageSize, filterList, startRow, stopRow)
    resultList.map(result => {
      HBaseUtil.toStr(result.getRow) match {
        case rowkey if (rowkey.split(":").length == 4) =>
          PiUserLogDay(rowkey,
            rowkey.split(":")(1),
            rowkey.split(":")(2),
            rowkey.split(":")(3),
            result.size())
        case _ => PiUserLogDay("", "", "", "")
      }
    }).filter(_.rowkey.nonEmpty)
  }

  /**
    * 某个 rowkey 中的所有日志
    */
  def listLogsByRowkey(rowkey: String, hasDocumentId: Boolean): List[PiUserLog] = {
    if (StringUtils.isEmpty(rowkey)) {
      return List()
    }
    // 全部列出来，采用前端分页
    val userLogs = new mutable.MutableList[PiUserLog]()
    val result = HBaseUtil.get(HBaseContent.piUserLogTableName, rowkey)
    result.rawCells.foreach(cell => {
      try {
        val valueJson = Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(cell)))
        if (!hasDocumentId || (valueJson \ "document_id").asOpt[String].getOrElse("").nonEmpty) {
          userLogs += PiUserLog(
            (valueJson \ "atime").asOpt[Long].getOrElse(0),
            (valueJson \ "account").asOpt[String].getOrElse(""),
            (valueJson \ "action").asOpt[String].getOrElse(""),
            (valueJson \ "document_id").asOpt[String].getOrElse(""),
            (valueJson \ "content").asOpt[String].getOrElse("")
          )
        }
      } catch {
        case e: Exception =>
          logger.error(e.getMessage, e)
      }
    })
    userLogs.toList
  }

  /**
    * 某个用户 tags 对应日志列表
    */
  def findLogByUserTags(userTags: List[(String, String)], hasDocumentId: Boolean, page: Int = 0, pageSize: Int = 10): List[PiUserLog] = {
    val results = userTags.map { tag =>
      val startRow = HBaseContent.piUserLogRowkeyPrefix(tag._1, tag._2)
      val endRow = startRow + "~"
      (startRow, endRow)
    }.filter(_._1.nonEmpty).par.flatMap { f =>
      HBaseUtil.scanDataBySmallPageNum(HBaseContent.piUserLogTableName, 1, pageSize, null, f._1, f._2)
    }.seq.toList

    val userLogs = new mutable.MutableList[PiUserLog]()
    results.foreach(result => {
      val rowkey = HBaseUtil.toStr(result.getRow)
      result.rawCells.foreach(cell => {
        try {
          val valueJson = Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(cell)))
          if (!hasDocumentId || (valueJson \ "document_id").asOpt[String].getOrElse("").nonEmpty) {
            userLogs += PiUserLog(
              (valueJson \ "atime").asOpt[Long].getOrElse(0),
              (valueJson \ "account").asOpt[String].getOrElse(""),
              (valueJson \ "action").asOpt[String].getOrElse(""),
              (valueJson \ "document_id").asOpt[String].getOrElse(""),
              (valueJson \ "content").asOpt[String].getOrElse("")
            )
          }
        } catch {
          case e: Exception =>
            logger.error(e.getMessage, e)
        }
      })
    })
    userLogs.toList
  }

}
