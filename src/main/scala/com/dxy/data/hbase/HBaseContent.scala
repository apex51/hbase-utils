package com.dxy.data.hbase

import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.Date

import com.typesafe.config.ConfigFactory

/**
  * Created by jianghao on 17/2/6.
  */
object HBaseContent {

  val hbaseConfig = ConfigFactory.load("hbase.conf")

  val zookeeperClientPort = hbaseConfig.getString("hbase.zookeeper.property.clientPort")
  val zookeeperQuorum = hbaseConfig.getString("hbase.zookeeper.quorum")

  val md5 = MessageDigest.getInstance("MD5")
  val simpleDateFormat = new SimpleDateFormat("yyyyMMdd")

  val piContentTableName = "pi_document_content"
  val piContentColumnFamily: Array[String] = Array("c", "o", "k") // content, original, keywords

  val piUserInfoTableName = "pi_user_info"
  val piUserInfoColumnFamily: Array[String] = Array("f")
  val piUserInfoColumn_userId: String = "dxyid"

  val piUserLogTableName = "pi_user_log"
  val piUserLogColumnFamily: Array[String] = Array("f")

  val piDoctorIndexTableName = "pi_doctor_index"
  val piDoctorIndexColumnFamily: Array[String] = Array("f")
  val piDoctorIndexColumn_influenceTime = "it" // 当天影响力的更新时间
  val piDoctorIndexColumn_activityTime = "at" // 当天活跃度的更新时间

  val piDoctorBaseInfluenceTableName = "pi_doctor_index_bi"
  val piDoctorBaseInfluenceColumnFamily: Array[String] = Array("f")
  val piDoctorBaseInfluenceColumn_baseInfluenceScore = "bi" // 当天的影响力的基础分

  val piUserContentTableName = "pi_user_content"
  val piUserContentColumnFamily: Array[String] = Array("f")

  val piDocumentUserActionTableName = "pi_document_user_action"
  val piDocumentUserActionColumnFamily: Array[String] = Array("f")

  // user tag 类型
  val TAG_TYPE_MC = "MC"
  val TAG_TYPE_C = "C"
  val TAG_TYPE_U = "U"

  // Function: stringToMD5.
  def stringToMD5(dxyid: String) = md5.digest(dxyid.getBytes("UTF-8")).map("%02x".format(_)).mkString

  def piContentRowkey(documentId: String): String = {
    documentId.hashCode.abs.toString.take(6).reverse + ":" + documentId
  }

  def piUserInfoRowkeyPrefix(userId: String): String = {
    stringToMD5(userId)
  }

  def piUserInfoRowkey(userId: String, tagType: String, tag: String = ""): String = {
    tagType match {
      case TAG_TYPE_MC =>
        piUserInfoRowkeyPrefix(userId) + ":" + tagType + ":" + tag
      case TAG_TYPE_C =>
        piUserInfoRowkeyPrefix(userId) + ":" + tagType + ":" + tag
      case TAG_TYPE_U =>
        piUserInfoRowkeyPrefix(userId) + ":" + tagType + ":" + stringToMD5(userId)
      case _ =>
        println("user info tag 错误：" + tag)
        ""
    }
  }

  def piUserLogRowkeyPrefix(tagType: String, tag: String): String = {
    if (tagType.equals(TAG_TYPE_MC) || tagType.equals(TAG_TYPE_C) || tagType.equals(TAG_TYPE_U)) {
      (tagType + ":" + tag).hashCode.abs.toString.take(6).reverse + ":" + tagType + ":" + tag
    } else {
      println("user log tag 错误：" + tag)
      ""
    }
  }

  def piUserLogRowkey(tagType: String, tag: String, day: Date): String = {
    piUserLogRowkeyPrefix(tagType, tag) + ":" + simpleDateFormat.format(day)
  }

  def piDoctorIndexRowkeyPrefix(userId: String): String = {
    stringToMD5(userId)
  }

  def piDoctorIndexRowkey(userId: String, day: Date): String = {
    piDoctorIndexRowkeyPrefix(userId) + ":" + simpleDateFormat.format(day)
  }

  def piDoctorBaseInfluenceRowkeyPrefix(userId: String): String = {
    stringToMD5(userId)
  }

  def piDoctorBaseInfluenceRowkey(userId: String, day: Date): String = {
    piDoctorBaseInfluenceRowkeyPrefix(userId) + ":" + simpleDateFormat.format(day)
  }

  // def getDateTime(day: Date): Long = {
  //   100000 - day.getTime / 86400000
  // }

  def piUserContentRowkeyPrefix(userId: String): String = {
    stringToMD5(userId)
  }

  def piUserContentRowkey(userId: String, day: Date): String = {
    piUserContentRowkeyPrefix(userId) + ":" + simpleDateFormat.format(day)
  }

  def piDocumentUserActionRowkeyPrefix(documentId: String): String = {
    documentId.hashCode.abs.toString.take(6).reverse + ":" + documentId
  }

  def piDocumentUserActionRowkeyPrefixWithDate(documentId: String, day: Date): String = {
    piDocumentUserActionRowkeyPrefix(documentId) + ":" + simpleDateFormat.format(day)

  }

  def piDocumentUserActionRowkey(documentId: String, day: Date, tagType: String, tag: String): String = {
    if (tagType.equals(TAG_TYPE_MC) || tagType.equals(TAG_TYPE_C) || tagType.equals(TAG_TYPE_U)) {
      piDocumentUserActionRowkeyPrefixWithDate(documentId, day) + ":" + tagType + ":" + tag
    } else {
      println("document user action tag 错误：" + tag)
      ""
    }
  }

}
