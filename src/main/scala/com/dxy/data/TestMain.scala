package com.dxy.data

import java.util.Date

import com.dxy.data.hbase.daos.PiDoctorIndexDao
import com.dxy.data.hbase.models.PiDoctorIndex
import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import org.apache.hadoop.hbase.CellUtil
import org.apache.hadoop.hbase.client.Result
import org.slf4j.{Logger, LoggerFactory}

/**
  * Hello world!
  *
  */
object TestMain {

  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  def main(args: Array[String]): Unit = {
    try {
      // HBaseUtil.createTable(HBaseContent.piDoctorIndexTableName, HBaseContent.piDoctorIndexColumnFamily)

      val userId = "firefly0512"
      val doctorIndexScoreMap = Map[String, PiDoctorIndex](
        "10002-4_1" -> PiDoctorIndex("10002-4_1", 5, 0, 0, 5),
        "10002-4_2" -> PiDoctorIndex("10002-4_2", 4, 0, 0, 4),
        "10002-4_7-1" -> PiDoctorIndex("10002-4_7-1", 10, 0, 0, 1)
      )
      new PiDoctorIndexDao().putDoctorIndexDay(userId, new Date(), doctorIndexScoreMap)

      //扫描表，获取前20行
      val results: List[Result] = HBaseUtil.scanDataBySmallPageNum(HBaseContent.piDoctorIndexTableName, 1, 20)
      results.foreach(result => {
        result.rawCells().foreach(cell => {
          logger.info("RowName:" + new String(CellUtil.cloneRow(cell)) + "")
          logger.info("Timetamp:" + cell.getTimestamp + "")
          logger.info("column Family:" + new String(CellUtil.cloneFamily(cell)) + "")
          logger.info("qualifier:" + new String(CellUtil.cloneQualifier(cell)) + "")
          logger.info("value:" + new String(CellUtil.cloneValue(cell)) + "")
          logger.info("---------------")
        })
        println("sdfsdfsdfasdfasdfasdfasdfdsf")
      })
    } catch {
      case e: Exception => println(e.getMessage)
    }
  }

}
